# dbcparse

Splits a databricks dbc-archive notebook into a student notebook, a solutions notebook, and a bunch of private test files based 
on simple yaml headers

```
Usage: dbcparse-exe INPUT OUTPUT-DIR
  Split databricks archive
```

usage example: `$ stack exec dbcparse-exe mynotebook.dbc results`, results will now contain two dbc archives, one containing 
student solutions and one containing answers, and a folder containing all private tests

yaml headers look like:

Cells only included in exercise notebook
```
//% tag: EXERCISE
CODE
```

Cells only included in answers notebook
```
//% tag: ANSWER
CODE
```

Cells containing tests (currently included in both exercise and answers notebooks)
```
//% tag: TEST
CODE
```

Cells containing private tests (not included in any notebook but are saved as JSON files)
```
//% tag: PRIVATE_TEST
//% title: title of test
//% success: Message on success
//% failure: Message on failure
CODE
```

One may use
```
//# TAG
```
as a shorthand for
```
//% tag: TAG
```
